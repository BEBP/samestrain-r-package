% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/simulPopBMCMC.R
\name{simulPopBMCMC}
\alias{simulPopBMCMC}
\title{run MCMC to estimation duration of outbreak or number of mutations per site per year}
\usage{
simulPopBMCMC(
  all_parms,
  dates,
  snp,
  n.isolat,
  n.gene,
  gene.length,
  mutation.user,
  duration,
  type,
  step = "day",
  rateB = NULL,
  n.iter = 10000,
  burnin = 2000,
  chain = 3,
  toEstimate,
  returnCompleteResult = FALSE
)
}
\arguments{
\item{all_parms}{vector of initial parameters, first one is the middle, second is the min and third is the max}

\item{dates}{samples dates in a data frame}

\item{snp}{snp pairwise distance matrix}

\item{n.isolat}{number of isolates}

\item{n.gene}{number of genes, must be 1 for a complete sequence}

\item{gene.length}{length of the sequence or average length of genes}

\item{mutation.user}{number of mutations per site per year}

\item{duration}{duration of outbreak}

\item{type}{must be SNP or MLST}

\item{step}{must be day month or year}

\item{rateB}{bottleneck rates between each samples dates, could be NULL if no bottlneck}

\item{n.iter}{number of iteration for MCMC at 10000 by default}

\item{burnin}{burnin of MCMC at 2000 by default}

\item{chain}{number of MCMC chain}

\item{toEstimate}{must be "duration" or "mutation"}

\item{returnCompleteResult}{TRUE for MCMC chain FALSE without}
}
\value{
chains result of the modMCMC from FME package
}
\description{
run MCMC to estimation duration of outbreak or number of mutations per site per year
}
\examples{

}
