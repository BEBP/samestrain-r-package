% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/add.R
\name{add.n.dates}
\alias{add.n.dates}
\title{add number of the date for simulation}
\usage{
add.n.dates(df, timeStep = "day", min = NULL, duration_timeStep = 1)
}
\arguments{
\item{df}{data frame with samples dates}

\item{timeStep}{time step "day" month or year}

\item{min}{minimum date; if min=NULL the minimum date chosen is the minimum sample date}

\item{duration_timeStep}{1 for day, 30.44 for month or 365 for year}
}
\value{
df with number of the samples dates for simulation
}
\description{
add number of the date for simulation
}
\examples{
data(dates)
dates<-add.n.dates(dates, "day")

}
