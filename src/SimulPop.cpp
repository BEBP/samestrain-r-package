
//' Wright Fisher forward model of bacteria evolution
//'
//' @name Simulation
//' 
//' @return simulation of outbreaks


#include <RcppArmadillo.h>
#include <Rcpp.h>
#include <algorithm>
#include <string>
#include <boost/algorithm/string.hpp>
#include <boost/random.hpp>



#ifdef _OPENMP
#include <omp.h>
#endif

// [[Rcpp::depends(RcppArmadillo)]]

// [[Rcpp::plugins(openmp)]]

// [[Rcpp::plugins(unwindProtect)]]

// [[Rcpp::depends(BH)]]

// [[Rcpp::plugins("cpp11")]]

using namespace Rcpp;
using namespace arma;

class sequence{
public:
  int geneLength;
  int ngene;
  std::vector<int> snp;
  std::map<int, int> allele; //en clé le numéro du gène et en valeurs l'allele qu'il a
  int label = 1;
  
  sequence(int geneLength, int ngene);
  ~sequence();
  sequence(const sequence &g2){allele=g2.allele; geneLength=g2.geneLength; ngene=g2.ngene; label=g2.label; snp=(g2.snp);}
  sequence(sequence* g2){allele=g2->allele; geneLength=g2->geneLength; ngene=g2->ngene; label=g2->label; snp=(g2->snp);}
  
  void addAsnp(int position);
  void addNewAllele(int numGene, int newA);
  void changeLabel(int newLab);
  std::vector<int> getSNP(){return snp;}
  std::map<int, int> getAllele(){return allele;}
  int getGeneLength(){return geneLength;}
  int getNGene(){return ngene;}
  int getLabel(){return label;}
  void setGeneLength(int geneLength_){geneLength = geneLength_;}
  void setLabel(int label_){label = label_;}
  void setNGene(int ngene_){ngene = ngene_;}
  
};

sequence::~sequence(){
  
}

sequence::sequence(int geneLength_, int ngene_):geneLength(geneLength_), ngene(ngene_){
  
}
void sequence::addAsnp(int position_){
  snp.push_back(position_);
}

void sequence::addNewAllele(int numGene, int newA){
  allele[numGene] = newA;
}

void sequence::changeLabel(int newLab){
  label = newLab;
}

class bacteria{
public:
  sequence* seq;
  int ngene;
  int number;
  std::string letter;
  
public: 
  bacteria(int ngene, int geneLength, std::string name);
  bacteria(int ngene, int geneLength);
  bacteria(const bacteria &b2);
  bacteria(bacteria* b2);
  ~bacteria();
  sequence* getSequence(){return seq;}
  std::string getLetter(){return letter;}
  int getNumber(){return number;}
  void setNumber(int nb){number = nb;}
  std::vector<int> getAllSnpPosition();
  std::vector<std::string> getAllAllele(); 
  
};

std::vector<int> bacteria::getAllSnpPosition(){
  std::vector<int> res;
  if(!seq->getSNP().empty()){
    std::vector<int> snps = seq->getSNP();
    res.insert(res.end(),snps.begin(), snps.end());
  }
  return res;
}

std::vector<std::string> bacteria::getAllAllele(){
  std::vector<std::string> res;
  std::map<int, int> alleles = seq->getAllele();
  if(!alleles.empty()){
    for(auto const& elem:alleles){
      if(elem.second>1){
        std::string sep = ":";
        std::string a_str = std::to_string(elem.first) + sep + std::to_string(elem.second);
        res.push_back((a_str));
      }
      
    }
  }
  
  return res;
}

bacteria::~bacteria(){
  
  delete seq;
}

bacteria::bacteria(const bacteria &b2){
  ngene = b2.ngene;
  letter = b2.letter;
  number = b2.number;
  seq = new sequence(b2.seq);
}

bacteria::bacteria(bacteria* b2){
  ngene = b2->ngene;
  letter = b2->letter;
  number = b2->number;
  seq = new sequence(b2->seq);
  
}

bacteria::bacteria(int ngene_, int geneLength_):ngene(ngene_){
  seq = new sequence(geneLength_, ngene_);
}

bacteria::bacteria(int ngene_, int geneLength_, std::string name):ngene(ngene_), letter(name){
  seq = new sequence(geneLength_, ngene_);
}

class population{
public:
  std::vector<bacteria*> all_bacteria;
  int nisolat;
  int ngene;
  int geneLength;
  std::vector<int> all_positions;
  std::map<std::string, std::vector<int>> bacteria_identification;
  std::map<int, int> allAllele;
  std::map<int, std::pair<int,int>> positionsOfGenes;
  
public:
  ~population();
  population(int nisolat_);
  population(const population &pop2);
  population(population* pop2);
  population(int nisolat_, int ngene_, int geneLength_);
  int getNisolat(){return nisolat;}
  void mutation(double mutationRate, boost::mt19937& rng);
  void find_offsprings(double bottleneck, boost::mt19937& rng);
  std::vector<int> getAll_positions(){return all_positions;}
  std::vector<std::string> returnPopulationSNP();
  std::vector<std::string> returnSubPopulationSNP(int nb, boost::random::mt19937 rng);
  std::vector<std::string> returnPopulationAllele();
  std::vector<std::string> returnSubPopulationAllele(int nb, boost::random::mt19937 rng);
  std::map<int, int> getAllAllele(){return allAllele;}
  std::map<int, std::pair<int,int>> getPositionsOfGenes(){return positionsOfGenes;}
  int getNumGene(int position);
  
};

population::~population(){
  for(auto const& elem:all_bacteria){
    delete elem;
  }
  all_bacteria.clear();
}

population::population(int nisolat_):nisolat(nisolat_){
  
}

population::population(const population &pop2){
  nisolat = pop2.nisolat;
  ngene = pop2.ngene;
  geneLength = pop2.geneLength;
  all_positions = (pop2.all_positions);
  allAllele = pop2.allAllele;
  positionsOfGenes = pop2.positionsOfGenes;
  for(auto const& elem:pop2.all_bacteria){
    bacteria* b = new bacteria(elem);
    all_bacteria.push_back(b);
  }
}

population::population(population* pop2){
  nisolat = pop2->nisolat;
  ngene = pop2->ngene;
  geneLength = pop2->geneLength;
  all_positions = (pop2->all_positions);
  allAllele = pop2->allAllele;
  positionsOfGenes = pop2->positionsOfGenes;
  for(auto const& elem:pop2->all_bacteria){
    bacteria* b = new bacteria(elem);
    all_bacteria.push_back(b);
  }
}

std::string randomName(int n) 
{ 
  const int MAX = 26;
  char chain[MAX] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' }; 
  std::string res = ""; 
  for (int i = 0; i < n; i++){
    res = res + chain[rand() % MAX]; 
  }  
  return res; 
} 

population::population(int nisolat_, int ngene_, int geneLength_):nisolat(nisolat_), ngene(ngene_), geneLength(geneLength_){
  for(int i(0); i<nisolat; i++){
    int l = std::to_string(nisolat_).length();
    std::string name = randomName(l);
    auto it = bacteria_identification.find(name);
    while(it!=bacteria_identification.end()){
      name = randomName(l);
      it = bacteria_identification.find(name);
    }
    bacteria* b = new bacteria(ngene_, geneLength_, name);
    b->setNumber(0);
    all_bacteria.push_back(b);
    bacteria_identification[name].push_back(0); 
  }
  int pos1 = 0;
  int pos2 = geneLength;
  for(int i(1); i<ngene+1; i++){
    // allAllele[i].push_back(1);
    allAllele[i]=1;
    positionsOfGenes[i]=std::make_pair(pos1, pos2-1);
    pos1 = pos2;
    pos2 = pos2 + geneLength;
  }
  
  
}

int population::getNumGene(int position){
  auto it = positionsOfGenes.begin();
  bool find = false;
  int numG = 0;
  while(!find && it!=positionsOfGenes.end()){
    int premier = it->second.first;
    int second = it->second.second;
    if(position>= premier && position <=second){
      find = true;
      numG = (*it).first;
    }
    ++it;
  }
  
  return numG;
}


std::vector<int> returnUnifNumber(int first, int second, int tot, boost::random::mt19937& rng){
  boost::random::uniform_int_distribution<> distrib(first, second);
  boost::variate_generator<boost::random::mt19937&, boost::random::uniform_int_distribution<>> var_expo(rng, distrib);
  std::vector<int> res;
  for(int i(0); i<tot;i++){
    int n = var_expo();
    res.push_back(n);
  }
  return res;
}

double randomPoisson(double mean, boost::mt19937& rng){
  boost::random::poisson_distribution<> exp( mean );
  boost::variate_generator<boost::mt19937&,boost::random::poisson_distribution<> > var_expo( rng, exp);
  return var_expo();
}

void population::mutation(double mutationRate, boost::mt19937& rng){
  if((int)all_positions.size() < ((geneLength*ngene)-1)){
    double n_mutation_per_gene = randomPoisson(mutationRate, rng);
    if(n_mutation_per_gene>0){
      std::vector<int> bacteria_pop = returnUnifNumber(0, nisolat-1, n_mutation_per_gene, rng);
      for(auto const& elem:bacteria_pop){
        std::vector<int> position = returnUnifNumber(0, ((geneLength*ngene)-1), 1, rng);
        auto it = find(all_positions.begin(), all_positions.end(), (int)position[0]);
        while(it!=all_positions.end()){
          position = returnUnifNumber(0, ((geneLength*ngene)-1), 1, rng);
          it = find(all_positions.begin(), all_positions.end(), (int)position[0]);
        }
        int newPos = (int)position[0];
        all_positions.push_back(newPos);
        //nouvelle position, nouvel allele
        int numGenePos = getNumGene(newPos);
        int lastAllele = allAllele[numGenePos];
        int newAllele = lastAllele + 1;
        allAllele[numGenePos] = newAllele; //nouvel allele de ce gene dans la pop
        all_bacteria[(int)elem]->getSequence()->addNewAllele(numGenePos, newAllele);//nouvel allele de ce gene pour la sequence de la bacterie
        all_bacteria[(int)elem]->getSequence()->addAsnp(newPos);
        int old_label = all_bacteria[(int)elem]->getSequence()->getLabel();
        all_bacteria[(int)elem]->getSequence()->changeLabel(old_label + 1);
      }
    }
  }else{
    Rcout << " mutations limits " << std::endl;
  }
}

void population::find_offsprings(double bottleneck, boost::mt19937& rng){
  try{
    if(bottleneck==0.0 || bottleneck == -1){
      throw std::range_error("bottleneck size is not correct");
    }
  }catch(std::exception& __ex__){
    forward_exception_to_r(__ex__);
  }catch(...){
    ::Rf_error("c++ exception (unknown reason)");
  }
  std::vector<int> selected_isolate;
  if(bottleneck!=0.0){
    int bottIsol = (int)round(bottleneck*nisolat);
    if(bottIsol==0)bottIsol=1;
    selected_isolate = returnUnifNumber(0, (nisolat-1), bottIsol, rng);
  }
  std::vector<int> selected_isolate_int = selected_isolate;
  std::vector<bacteria*> new_bacteria;
  for(int i(0); i<nisolat; i++){
    int nbToKeep;
    if(selected_isolate_int.size()==1){
      nbToKeep = 0;
    }else{
      std::vector<int> toKeepInt = returnUnifNumber(0, selected_isolate_int.size(), 1, rng);
      nbToKeep = (int)toKeepInt[0];
    }
    int TKI = selected_isolate_int[nbToKeep];
    bacteria* b = new bacteria(all_bacteria[TKI]);
    int maxNbB = (*bacteria_identification[b->getLetter()].rbegin());
    b->setNumber(maxNbB+1);
    bacteria_identification[b->getLetter()].push_back(maxNbB+1);
    new_bacteria.push_back(b);
  }
  for(auto const& elem:all_bacteria){
    delete elem;
  }
  all_bacteria = new_bacteria;
}

std::vector<std::string> population::returnPopulationAllele(){
  std::vector<std::string> res;
  for(auto const& b:all_bacteria){
    sequence* sequence = b->getSequence();
    std::map<int, int> allele = sequence->getAllele();
    std::string sep = "_";
    std::string minSep = ":";
    std::string allele_str_gene;
    if(allele.size()==1){
      auto it = allele.begin();
      allele_str_gene=std::to_string(it->first) + minSep + std::to_string(it->second);
    }else if(allele.size()>1){
      auto it = allele.begin();
      allele_str_gene=std::to_string(it->first) + minSep + std::to_string(it->second);
      ++it;
      for(auto it2 = it; it2!=allele.end(); ++it2){
        allele_str_gene = allele_str_gene + sep + std::to_string(it2->first) + minSep + std::to_string(it2->second);
      }
    }else{
      allele_str_gene=".";
    }
    res.push_back(allele_str_gene);
  }
  return res;
}

std::vector<std::string> population::returnSubPopulationAllele(int nb, boost::random::mt19937 rng){
  std::vector<int> bacteria_index = returnUnifNumber(0, (all_bacteria.size()-1), nb, rng);
  std::vector<std::string> res;
  for(auto const& index:bacteria_index){
    bacteria* b = all_bacteria[index];
    sequence* sequence = b->getSequence();
    std::map<int, int> allele = sequence->getAllele();
    std::string sep = "_";
    std::string minSep = ":";
    std::string allele_str_gene;
    if(allele.size()==1){
      auto it = allele.begin();
      allele_str_gene=std::to_string(it->first) + minSep + std::to_string(it->second);
    }else if(allele.size()>1){
      auto it = allele.begin();
      allele_str_gene=std::to_string(it->first) + minSep + std::to_string(it->second);
      ++it;
      for(auto it2 = it; it2!=allele.end(); ++it2){
        allele_str_gene = allele_str_gene + sep + std::to_string(it2->first) + minSep + std::to_string(it2->second);
      }
    }else{
      allele_str_gene=".";
    }
    
    res.push_back(allele_str_gene);
  }
  return res;
}


std::vector<std::string> population::returnSubPopulationSNP(int nb, boost::random::mt19937 rng){
  std::vector<int> bacteria_index = returnUnifNumber(0, (all_bacteria.size()-1), nb, rng);
  std::vector<std::string> res;
  for(auto const& index:bacteria_index){
    bacteria* b = all_bacteria[index];
    sequence* sequence = b->getSequence();
    std::string snp_str_gene="";
    std::vector<int> snp = sequence->getSNP();
    std::string snp_str = "";
    std::string sep = "_";
    if(snp.size()==1){
      snp_str = std::to_string(snp[0]);
    }else if(snp.size()>1){
      snp_str = std::to_string(snp[0]);
      for(int j(1); j<(int)snp.size(); j++){
        snp_str = snp_str + sep + std::to_string(snp[j]) ;
      }
    }else{
      snp_str = ".";
    }
    snp_str_gene = snp_str;
    res.push_back(snp_str_gene);
  }
  return res;
}

std::vector<std::string> population::returnPopulationSNP(){
  std::vector<std::string> res;
  for(auto const& b:all_bacteria){
    sequence* sequence = b->getSequence();
    std::string snp_str_gene="";
    std::vector<int> snp = sequence->getSNP();
    std::string snp_str = "";
    std::string sep = "_";
    if(snp.size()==1){
      snp_str = std::to_string(snp[0]);
    }else if(snp.size()>1){
      snp_str = std::to_string(snp[0]);
      for(int j(1); j<(int)snp.size(); j++){
        snp_str = snp_str + sep + std::to_string(snp[j]) ;
      }
    }else{
      snp_str = ".";
    }
    
    snp_str_gene = snp_str;
    res.push_back(snp_str_gene);
  }
  return res;
}

inline int randWrapper(const int n) { return floor(unif_rand()*n); }

std::vector<std::string> findSamples(std::vector<std::string> bacteria, int nb){
  IntegerVector pool = seq(0,(bacteria.size()-1));
  std::random_shuffle(pool.begin(), pool.end(), randWrapper);
  IntegerVector index_pool = (pool[Range(1,nb)]);
  std::vector<std::string > new_bacteria;
  for(auto const& n:index_pool){
    new_bacteria.push_back(bacteria[(n)]);
  }
  return new_bacteria;
}


std::map<int, int> tableC(std::vector<std::string> x) {
  std::map<int, int> counts;
  
  int n = x.size();
  for (int i = 0; i < n; i++) {
    counts[std::stoi((x[i]))]++;
  }
  
  return counts;
}

std::map<int, int> tableC(std::vector<int> x) {
  std::map<int, int> counts;
  
  int n = x.size();
  for (int i = 0; i < n; i++) {
    counts[((x[i]))]++;
  }
  
  return counts;
}

std::map<std::string, int> tableS(std::vector<std::string> x) {
  std::map<std::string, int> counts;
  
  int n = x.size();
  for (int i = 0; i < n; i++) {
    std::string str = (x[i]);
    auto it = counts.find(str);
    if(it!=counts.end()){
      counts[str]++;
    }else{
      counts[str] = 1;
    }
    
  }
  
  return counts;
}

std::vector<std::pair<int, int>> combinaisonByTwo(IntegerVector seq){
  std::vector<std::pair<int, int>> res;
  for(int i(1); i<=seq.size(); i++){
    for(int j(1); j<=seq.size(); j++){
      if(i!=j){
        std::pair<int, int> p1 = std::make_pair(i,j);
        std::pair<int, int> p2 = std::make_pair(j,i);
        auto it1 = find(res.begin(), res.end(), p1);
        auto it2 = find(res.begin(), res.end(), p2);
        if(it1==res.end() && it2==res.end()){
          res.push_back(p1);
        }
      }
    }
  }
  return res;
}

void makeCombiUtil(std::vector<std::vector<int> >& ans, std::vector<int>& tmp, int n, int left, int k){ 
  // Pushing this vector to a vector of vector 
  if (k == 0) { 
    ans.push_back(tmp); 
    return; 
  } 
  
  for (int i = left; i <= n; ++i) 
  { 
    tmp.push_back(i); 
    makeCombiUtil(ans, tmp, n, i + 1, k - 1); 
    tmp.pop_back(); 
  } 
}

std::vector<std::vector<int> > makeCombi(int n, int k){ 
  std::vector<std::vector<int> > ans; 
  std::vector<int> tmp; 
  makeCombiUtil(ans, tmp, n, 1, k); 
  return ans; 
}

int numberOfsnpDiff(std::string a, std::string b){
  
  std::vector<std::string> stra;
  boost::split(stra,a,boost::is_any_of("_"));
  std::vector<std::string> strb;
  boost::split(strb,b,boost::is_any_of("_"));
  stra.insert(stra.end(), strb.begin(), strb.end());
  std::map<std::string, int> tab_stra = tableS(stra);
  
  int nbOfsnp = 0;
  
  for(auto const& e:tab_stra){
    if(e.first!="." && e.second==1){
      nbOfsnp = nbOfsnp + 1;
    }
  }
  
  return nbOfsnp;
}

double percentile(std::vector<double> &vectorIn, double percent)
{
  sort(vectorIn.begin(), vectorIn.end());
  auto nth = vectorIn.begin() + (percent*vectorIn.size())/100;
  std::nth_element(vectorIn.begin(), nth, vectorIn.end());
  return *nth;
}

bool checkBottleneck(std::vector<double> x){
  bool res = false;
  for(auto const& elem:x){
    if(elem!=-1.0){
      res = true;
    }
  }
  return(res);
}


//' @export
// [[Rcpp::export(rng = false)]] 
List simulation(int nisolat, int ngene, int geneLength, double mutationRate, int duration, bool SNPdetail, arma::Col<int> df, arma::Mat<double> df_bottleneck, std::string type="SNP", double seuil_percent=99.0, int t_thread=1){
  // try{
  //   if(bottleneck>1.0 || bottleneck < -1){
  //     throw std::range_error("bottleneck size is not correct");
  //   }
  // }catch(std::exception& __ex__){
  //   forward_exception_to_r(__ex__);
  // }catch(...){
  //   ::Rf_error("c++ exception (unknown reason)");
  // }
  
  //Find random number
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  srand((time_t)ts.tv_nsec);
  int tt = rand()*t_thread;
  boost::random::mt19937 generator(tt);
  
  //Find samples and bottleneck dates
  std::vector<int> swab_dates_number =  arma::conv_to< std::vector<int> >::from(df);
  std::vector<int> bottleneck_dates_number = arma::conv_to< std::vector<int> >::from(df_bottleneck.col(0));
  std::vector<double> bottleneck_rate_number = arma::conv_to< std::vector<double> >::from(df_bottleneck.col(1));

  if(ngene<0 || geneLength<0 || mutationRate<0.0 || duration < 0 ){
    throw std::range_error("parameters size issue");
  }
  if((nisolat*duration) < (int)swab_dates_number.size()){
    throw std::range_error("not enough isolate for simulation");
  }
  
  //Samples dates process
  std::map<int, int> counts = tableC(swab_dates_number);
  int maxDurationCounts = counts.rbegin()->first;
  int minDurationCounts = counts.begin()->first;
  std::map<int, int> countsAdjusted = counts;
  if(maxDurationCounts>duration || minDurationCounts < 1){
    throw std::range_error("no enough simulation dates");
  }
  
  std::string typeSTR= type;
  
  //Bottleneck samples dates process
  int nC = df_bottleneck.n_cols;
  int firstNum = df_bottleneck(0,0);
  std::map<int, double> countsBrate;
  std::map<int, int> countsBAdjusted;
  // check for bottleneck
  bool boolBottleneck = false;
  if(nC!=1){
    if(firstNum < 0 ){
      throw std::range_error("first bottleneck date is not correct");
    }
    boolBottleneck = checkBottleneck(bottleneck_rate_number);
  }

  if(boolBottleneck){
    for(int i(0); i< (int)bottleneck_dates_number.size(); i++){
      int d =((bottleneck_dates_number[i]));
      double n = ((bottleneck_rate_number[i]));
      countsBrate[d] = n;
    }
    std::map<int, int> countsB = tableC(bottleneck_dates_number);
    countsBAdjusted = countsB;
  }
  
  //population simulation
  population* p = new population(nisolat, ngene, geneLength);
  std::vector<std::string> out2; 
  std::map<int, std::vector<std::string>> resultSNP;
  int day = 0;
  for(int d(0); d<duration; d++){
    day = day + 1;
    p->mutation(mutationRate, generator);
    if(boolBottleneck && bottleneck_dates_number.size()!=0){
      std::map<int, int>::iterator itBmap = countsBAdjusted.find(day);
      std::map<int, double>::iterator itBrateMap = countsBrate.find(day);
      if(itBmap!=countsBAdjusted.end()){
        double newBottleneck = (*itBrateMap).second;
        if(newBottleneck!=-1 && newBottleneck!=0.0){
          p->find_offsprings(newBottleneck, generator);
        }
      }
    }
    
    
    std::map<int, int>::iterator itmap = countsAdjusted.find(day);
    if(itmap!=countsAdjusted.end()){
      
      std::vector<std::string> samples;
      int nbOfI = countsAdjusted[day];
      if(typeSTR=="SNP"){
        samples = p->returnSubPopulationSNP(nbOfI, generator);
        
      }else{
        samples = p->returnSubPopulationAllele(nbOfI, generator);
      }
      out2.insert(out2.end(), samples.begin(), samples.end()) ;
      if(SNPdetail)resultSNP[day] = samples;
      
    }
    
    
  }
  
  std::vector<std::vector<int>> test2 = makeCombi((int)swab_dates_number.size(), 2);
  
  std::vector<std::string> all_snp_differences;
  std::vector<double> snp_threshold;
  
  for(auto const& t:test2){
    std::vector<int> t_v = t;
    int n = numberOfsnpDiff(out2[t_v[0]-1], out2[t_v[1]-1]);
    all_snp_differences.push_back(std::to_string(n));
    snp_threshold.push_back((double)n);
    
  }
  
  std::map<int, int> mapDiff = tableC(all_snp_differences);
  double threshold = percentile(snp_threshold, seuil_percent);
  
  if(SNPdetail){
    return Rcpp::List::create(Rcpp::Named("map") = mapDiff, Rcpp::Named("threshold") = (double)threshold, Rcpp::Named("description") = NULL, Rcpp::Named("SNPdetail") =resultSNP);
  }else{
    return Rcpp::List::create(Rcpp::Named("map") = mapDiff, Rcpp::Named("threshold") = (double)threshold);
  }
}

//' @export
// [[Rcpp::export(rng = false)]] 
List multipleSim(int nisolat, int ngene, int geneLength, double mutationRate, int duration, bool SNPdetail, arma::Col<int> df, int nbOfSim, arma::Mat<double> df_bottleneck, int threads=1, std::string type="SNP", double seuil_percent=99.0){
  if(nbOfSim<0 ){
    throw std::range_error("not enough simulation");
  }
  List out(nbOfSim);
#ifdef _OPENMP
  if ( threads > 0 ){
    omp_set_num_threads( threads );
  }
#endif
  int i;
  
#pragma omp parallel for schedule(dynamic) 
  for(i =0; i<nbOfSim; i++){
    
#pragma omp critical
{
  out[i] = simulation(nisolat, ngene, geneLength, mutationRate, duration, SNPdetail, df, df_bottleneck, type, seuil_percent, i);
}
  }
  return out;
}


RCPP_MODULE(mod_population){
  using namespace Rcpp;
  
  function("simulation" , &simulation);
  function("multipleSim" , &multipleSim);
  
}
