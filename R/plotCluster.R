#' Graph of the cluster according to the cut-off
#'
#' @param real.data.snp.m SNP paiwise distance matrix
#' @param level cut-off
#' @param detail optional if the user wants outliers
#'
#' @return a cluster graph that gather outbreak cases and exclude outliers according to the cut-off
#' @export
#'
#' @examples
#' data(snp)
#' plotCluster(snp, 8)
#'
plotCluster<-function(real.data.snp.m, level, detail=FALSE){
  edges <- NULL
  wgts <- NULL
  isolates <- rownames(real.data.snp.m)
  for (i in seq(length(rownames(real.data.snp.m))-1)){
    for (j in seq(i+1, length(rownames(real.data.snp.m)))){
      if (real.data.snp.m[i,j]<=level){
        edges <- c(edges, rownames(real.data.snp.m)[[i]],rownames(real.data.snp.m)[[j]])
        wgts <- c(wgts, 1+1*(level-real.data.snp.m[i,j]))
        isolates <- isolates[isolates != rownames(real.data.snp.m)[[i]]]
        isolates <- isolates[isolates != rownames(real.data.snp.m)[[j]]]
      }
    }
  }
  cgraph <- igraph::graph(edges=edges, isolates=isolates, directed=F)
  igraph::E(cgraph)$weight <- wgts
  if(!detail){
    return(GGally::ggnet2(cgraph, node.color = "tomato", mode = "kamadakawai", label = TRUE, node.size = 10)+ggplot2::ggtitle(paste("Threshold: ",level, sep=""))+ggplot2::coord_equal())
  }else{
    p = GGally::ggnet2(cgraph, node.color = "tomato", mode = "kamadakawai", label = TRUE, node.size = 10)+ggplot2::ggtitle(paste("Threshold: ",level, sep=""))+ggplot2::coord_equal()
    return(list(p, isolates))
  }

}
