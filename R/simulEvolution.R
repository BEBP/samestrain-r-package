#' Function for simulation
#'
#' @param dates samples dates
#' @param n.isolat number of isolates per step
#' @param n.gene number of gene, must be 1 if complete sequence
#' @param gene.length length of the sequence or the average length of genes
#' @param mutation.user number of mutations per site per year
#' @param duration duration of outbreak
#' @param type must be SNP or MLST
#' @param step must be day, month or year
#' @param nsim number of simulations
#' @param ratesB bottleneck rates
#' @param verbose print the cut off
#'
#' @return the cut-off and the average distribution of pairwise SNP distance for the nsim simulation with the prediction interval at 95% for each points
#' @export
#'
#' @examples
#' data(dates)
#' n.isolat=500
#' n.gene=1
#' gene.length=4800000
#' mutation.user=0.00000012
#' duration=120
#' type="SNP"
#' step="day"
#' nsim=100
#' simulEvolution(dates, n.isolat, n.gene, gene.length, mutation.user, duration, type, step, nsim)
#' 
simulEvolution<-function(dates, n.isolat, n.gene, gene.length, mutation.user, duration, type, step, nsim=1, seuil_percent=99.0, ratesB=NULL, verbose=TRUE){
  duration_timeStep = timeStep_function(step, "duration_timeStep")
  dates_diff = ceiling((max(dates$dates) - min(dates$dates))/duration_timeStep)
  tryCatch({
    
    if(!(dates_diff<duration)){
      stop("warning message") 
    }else{
      cores <- parallel::detectCores()
      
      firstD = max(dates$dates)-(duration*duration_timeStep)
      mutation.rate = findMutationRate(mutation.user, n.isolat, gene.length, n.gene, step)
      if(is.null(ratesB)){
        data.bottleneck= as.matrix(t(c(0,-1)))
      }else{
        data.bottleneck= as.matrix(buildBottleneckDF(dates, firstD, ratesB, step, duration_timeStep))
      }
      dates<-add.n.dates(dates, step, firstD, duration_timeStep)
      data.to.compare = dates
      
      SIM<-multipleSim(n.isolat, n.gene, gene.length, mutation.rate, duration, FALSE, as.numeric(data.to.compare$n.dates), nsim, data.bottleneck, cores-1, type, seuil_percent)
      
      threshold = round(mean(unlist(lapply(SIM, function(x){return(x$threshold)}))),0)
      sim.data = lapply(SIM, function(x){return(x$map)})
      
      res.ggplot <- multipleSimulationDistributionOfSNP(sim.data, nsim)
      if(verbose)print(paste("Cut-off : ", threshold), sep="")
      return(list(threshold, res.ggplot))
      
    }
  },error=function(cond){
    message("difftime is bigger than duration")
    return(0)
  })
  
  
}