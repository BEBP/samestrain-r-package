#' Find step time
#'
#' @param timeStep must be "day" "month" or "year"
#' @param type "d_timeStep" to convert the number of mutations per site per year or "duration_timeStep" for the opposite or "both"
#'
#' @return d_timeStep or duration_timeStep or both
#' @export
#'
#' @examples
#' timeStep_function("day", "d_timeStep")
timeStep_function<-function(timeStep, type="d_timeStep"){
  
  if(timeStep=="day"){
    d_timeStep = 365
    duration_timeStep = 1
  }else if(timeStep=="month"){
    d_timeStep = 12
    duration_timeStep = 30.44
  }else if(timeStep=="year"){
    d_timeStep = 1
    duration_timeStep = 365.25
  }
  
  if(type=="d_timeStep"){
    return(d_timeStep)
  }else if(type=="duration_timeStep"){
    return(duration_timeStep)
  }else{
    return(list(d_timeStep, duration_timeStep))
  }
}