
#' built a data frame with bottleneck dates and bottleneck rates needed
#'
#' @param df_x samples dates in a data frame
#' @param FD first date of the simulation
#' @param ratesB bottleneck rates in each bottleneck dates
#' @param timeStep time step must be day, month or year
#' @param duration_timeStep must be 1 for day, 30.44 for month or 365.25 for year
#'
#' @return
#' @export
#'
#' @examples
#' data(dates)
#' data.bottleneck= buildBottleneckDF(dates, as.Date("2020-06-30", origin="1970-01-01"), rep(0.1, length(unique(dates$dates))))
#'
buildBottleneckDF<-function(df_x, FD, ratesB, timeStep="day", duration_timeStep=1){
  bottleneck_dates <- NULL
  if(length(unique(dates$dates))>1){
    bottleneck_dates<-selectDatesBottleneck(unique(df_x$dates))
  }
  
  if(((min(df_x$dates)-(FD+1))>1)){
    supp_dates<-sample(seq(FD+1, min(df_x$dates)-1, 1), 1)
    bottleneck_dates<-c(supp_dates, bottleneck_dates)
  }else{
    bottleneck_dates<-c(FD+1, bottleneck_dates)
  }

  bottleneck_dates<-bottleneck_dates[order(bottleneck_dates)]
  bottleneck_dates<-add.n.dates(data.frame(dates=bottleneck_dates, stringsAsFactors = F),timeStep, FD, duration_timeStep)

  if(timeStep=="day"){
    bottleneck_dates<-data.frame(dates = bottleneck_dates$dates,n.dates = as.character(unique(bottleneck_dates$n.dates)), n.rates = as.character(ratesB), stringsAsFactors = F)
  }else{
    bottleneck_dates<-data.frame(n.dates = as.character(unique(bottleneck_dates$n.dates)), n.rates = as.character(ratesB), stringsAsFactors = F)
  }

  data.bottleneck=bottleneck_dates
  return(data.bottleneck)
}
